import * as express  from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';

import Pokemon from './db';
import router from './routes/index';


let app = express();
let port = process.env.PORT || 3000;

// Body-Parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// cors
app.use(cors());

// Static dir
app.use(express.static('dist'));

// Sync DB
Pokemon.sync({ force: true })
	.then(() => {
		console.log('Models is ready!')

		Pokemon.bulkCreate([
			{
				name: 'Pikachu',
				price: 25,
				picture: 'https://vignette2.wikia.nocookie.net/pokemon/images/b/b1/025Pikachu_XY_anime_3.png/revision/latest?cb=20140902050035',
				stock: 1000
			},
			{
				name: 'Bulbasaur',
				price: 15,
				picture: 'http://vignette1.wikia.nocookie.net/pokemon/images/b/b8/001Bulbasaur_Dream.png/revision/latest?cb=20140903033758',
				stock: 600
			},
			{
				name: 'Charmander',
				price: 18,
				picture: 'http://vignette4.wikia.nocookie.net/pokemon/images/9/9b/004Charmander_AG_anime.png/revision/latest?cb=20140603214916',
				stock: 900
			}
		])
		.then(pokemon => console.log(pokemon))

	});




// Routes
app.use('/', router);

// Running server
app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});