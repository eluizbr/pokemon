import * as Sequelize from 'sequelize';

let dbName = 'pokemons';

let db = new Sequelize(dbName, null, null, {
	dialect: 'sqlite'
})

// DB Model
let Pokemon = db.define(dbName, {
	name: {
		type: Sequelize.STRING,
		allowNull: false
	},
	price: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	picture: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	stock: {
		type: Sequelize.INTEGER,
		allowNull: true,
		defaultValue: 1
	}
})


export default Pokemon;