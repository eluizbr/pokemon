/**
 * Realiza o tratamento dos erros encontrados durante as criação do pokemon.
 * Retorna um array de objetos contendo os erros encontrados.
 * @param {array} error Array contendo os erros
 */
let errorHandling = (error: Array<Object>) => {

	let result: Array<Object> = [];

	error.map(elem => {
		if(elem['path'] == 'name') {
			console.log(elem)
			let msg = {
				message: 'O nome é obrigatório!',
				value: elem['value']
			}
			result.push(msg);
		}
		if(elem['path'] == 'price') {
			console.log(elem)
			let msg = {
				message: 'O preço é obrigatório!',
				value: elem['value']
			}
			result.push(msg);
		}

	});

	return result;
}

export default errorHandling;