import * as express from 'express';
import { Request, Response } from 'express';

import pagarmeRequest from '../pagarmeRequest';
import Pokemon from '../db';
import settings from '../settings';
import updateStock from '../updateStock';

const pagarme = require('pagarme')

let buyPokemon = express.Router();

buyPokemon
	.post('/buy-pokemons', (req: Request, res: Response) => {
		Pokemon.findOne({
			where: { name: req.body.name }
		})
			.then(pokemon => {

				// The new stock for the Pokemon
				let newStock = pokemon['stock'] - req.body.quantity;

				if (pokemon['stock'] < req.body.quantity) {
					return res.status(400).json({ error: 'Not enought ' + pokemon['name'] + ' in stock: ' + pokemon['stock'] })
				}

				// PagarMe lib
				pagarme.client.connect({ api_key: settings.api_key })
					.then(client => client.security.encrypt(settings.card))
					.then(card_hash => {

						// Make PagarMe request
						pagarmeRequest(pokemon, req.body, card_hash)
							.then(body => {
								updateStock(body, req.body.name, newStock);

								res.status(200).json({ currentStock: newStock, data: body });
							})
							.catch(err => res.status(err.response.statusCode).json(JSON.parse(err.response.body)));
					});
			})
	});

export default buyPokemon;