import * as express from 'express';
import { Request, Response } from 'express';
import Pokemon from '../db';
import errorHandling from '../errorHandling';

let PutPokemons = express.Router();

PutPokemons
	.put('/create-pokemons', (req: Request, res: Response) => {
		Pokemon.create(req.body)
			.then(pokemon => res.status(201).json(pokemon))
			.catch(err => res.status(403).json(errorHandling(err.errors)));
	});


export default PutPokemons;