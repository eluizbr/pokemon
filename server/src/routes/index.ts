import * as express from 'express';

import getPokemons from './pokemons-get';
import PutPokemons from './pokemons-put';
import buyPokemon from './buy-pokemons';

let router = express.Router();

router
	.get('/get-pokemons', getPokemons)
	.post('/buy-pokemons', buyPokemon)
	.put('/create-pokemons', PutPokemons);


export default router;