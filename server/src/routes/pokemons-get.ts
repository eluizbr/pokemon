import * as express from 'express';
import { Request, Response } from 'express';
import Pokemon from '../db';

let getPokemons = express.Router();

getPokemons
	.get('/get-pokemons', (req: Request, res: Response) => {
		Pokemon.findAll()
			.then(pokemons => res.status(200).json(pokemons));
	});


export default getPokemons;