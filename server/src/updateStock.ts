import Pokemon from './db';

/**
 * Update the pockmon stock
 * @param {object} payment Return from PagarMe
 * @param {string} name Name of Pokemon
 * @param {number} stock New stock for the Pokemon
 */

let updateStock = (payment: Object, name: string, stock: number) => {
    if(payment['status'] === 'paid') {
        Pokemon.update(
            { stock },
            { where: { name } },
        )
        .then(stock => console.log(stock))
    }
}

export default updateStock;