import * as request from 'request-promise';
import settings from './settings';

/**
 * Realiza a compra do Pokemon
 * @param {object} pokemon Dados do pokemon
 * @param {object} body Dados da compra
 */

let pagarmeRequest = (pokemon: Object, body: Object, cardHash: string) => {
	console.log(pokemon['price'], parseInt(body['quantity']))
	let config = {
		url: settings.url,
		method: 'post',
		json: true,
		body: {
			api_key: settings.api_key,
			amount: pokemon['price'] * parseInt(body['quantity']) * 1000,
			card_hash: cardHash,
			metadata: {
				product: 'pokemon',
				name: pokemon['name'],
			}
		}
	}

	return request(config);
};

export default pagarmeRequest;

